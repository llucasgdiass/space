package com.example.lucas.space;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class Activity2_confRede extends AppCompatActivity {
    // TextView log;
    EditText endereco;  EditText port; int porta;

    ClienteSocket socket; Button botao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity2_conf_rede);
        endereco = (EditText) findViewById(R.id.editText);
        port = (EditText) findViewById(R.id.editText2);
        //log = (TextView) findViewById(R.id.textView3);
        botao = findViewById(R.id.button);

        //======================== PARA INCLUIR O QUE ESTA NO ARQUIVO =================
        Intent intent = getIntent();
        String receiveMain = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
        if(receiveMain != null)
        {
            String[] dados = receiveMain.split("\n"); // .split() separa a String em linhas quando encontra "\n"
            endereco.setText(dados[0]);
            port.setText(dados[1]);

        }
        //=============================================================================
    }

    public void bt_conectar(View view)
    {

        // Para testes --> e = "Botão pressionado\n"+ e;
        porta = Integer.parseInt(port.getText().toString());
        socket = new ClienteSocket(endereco.getText().toString(), porta);
        socket.execute();
        botao.setText(gravar());

    }

    public String gravar()
    {
        final String ARQUIVO = "config.txt";
        String saida;
        try
        {
            FileOutputStream textOut = openFileOutput(ARQUIVO, Context.MODE_PRIVATE);
            saida = endereco.getText() + "\n" + Integer.parseInt(port.getText().toString())+"\n";
            //textOut.write(0);
            textOut.write(saida.getBytes());
            return "Salvo";
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
            return  "Nenhum arquivo";


        }
        catch (IOException e)
        {
            e.printStackTrace();
            return  "Erro.";
        }


    }

}
