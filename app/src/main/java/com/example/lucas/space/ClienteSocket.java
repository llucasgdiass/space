package com.example.lucas.space;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class ClienteSocket extends AsyncTask<Void, Void, Boolean>
{
    public  String id = "";

    public int port; public String endereco;

    public ClienteSocket(String ip, int port)   //construtor
    {
        this.endereco = ip;      this.port = port;
    }

    Socket socket = null;       //Socket cliente
    PrintWriter out = null;     //Escreve no socket
    BufferedReader in = null;   //Le do socket

    //----------------------------------------------------------------------------


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        id = "Conectando...";

    }

    @Override
    protected Boolean doInBackground(Void... params) {
        try {
            socket = new Socket(endereco, port); //Atenção para o IP
            out = new PrintWriter(socket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            //Activity2_confRede.aux = Activity2_confRede.aux + "Conectado\n";
            id = "Conectado";


            return true;

        } catch (UnknownHostException e) {

            //Activity2_confRede.aux = Activity2_confRede.aux + "Não foi possível criar a conexão com o servidor.\n";
            id = "Verifique o host, endereço indisponível";

            return false;

        } catch (IOException e) {

            //Activity2_confRede.aux = Activity2_confRede.aux + "Não foi possível criar a conexão com o servidor.\n";
            id = "Impossível criar conexão";

            return false;
        }



    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);

    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        MainActivity.popup1.setText(id).show();

    }

    public void closeAll() throws IOException {
        socket.close();
        out.close();
        in.close();
    }



}
