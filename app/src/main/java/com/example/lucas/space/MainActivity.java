package com.example.lucas.space;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    public String IP = "";
    public String PORT = "";

    public static TextView sup;
    String aux2 = null;
    public static final String EXTRA_MESSAGE = "com.example.space.MESSAGE";
    ClienteSocket socket;
    SocketServidor servidor;
    public static  Snackbar popup1;


    //=============================================== onCreate ===================================================================


    //Os métodos abaixo são dos botões de menu da Barra superior (Action Bar)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        sup = findViewById(R.id.textViewSuperior);

    }

    //================================================ onStart ======================================================================

    @Override
    protected void onStart() {
        super.onStart();

        //-------------------- VERIFICA O ARQUIVO CASO ELE EXISTA ----------------------
        aux2 = ler_arquivo();
        if(! aux2.equals(null))
        {
            String[] dados = aux2.split("\n"); // .split() separa a String em linhas quando encontra "\n"
            IP = dados[0];
            PORT = dados[1];
        }
        else
        {

        }
        //-------------------------------------------------------------------------------*/

        //--------------- INICIA O SOCKET E TESTA SE O OUTRO HOST ESTÁ ONLINE ----------------

        int port;
        try{
            port = Integer.parseInt(PORT);
        }catch (NumberFormatException e)
        {
            e.printStackTrace();
            port = 0;
        }

        socket = new ClienteSocket(IP, port);
        socket.execute();


        popup1 = Snackbar.make(findViewById(R.id.myCoordinatorLayout), socket.id, Snackbar.LENGTH_SHORT);
        popup1.show();

        //---------------------------------------------------------------------------------------

        servidor = new SocketServidor(5000);
        servidor.execute();
    }


    //========================================================== MENUS ==================================================================

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_connect)
        {
            Intent intent = new Intent(this, Activity2_confRede.class);
            intent.putExtra(EXTRA_MESSAGE, aux2);
            startActivity(intent);

            return true;
        }
        if (id == R.id.action_about)
        {
            //ações aqui
            //ler_arquivo()
            Intent intent = new Intent(this, AboutActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //=================================================== LÊ DO ARQUIVO =====================================================================
    public String ler_arquivo()
    {
        final String ARQUIVO = "config.txt";
        String snack = ""; String aux = null;

        try
        {
            File arquivo = getFileStreamPath(ARQUIVO);
            if(arquivo.exists())
            {
                FileInputStream entrada = openFileInput(ARQUIVO);
                int tamanho = entrada.available();
                byte bytes[] = new byte[tamanho];
                entrada.read(bytes);
                aux = new String(bytes);   //aux já recebe todos os caracteres, lembrar que string é vetor de caracteres
                //sup.setText(aux);       //
                entrada.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            snack = "Arquivos de configuração essenciais estão faltando.";

        } catch (IOException e) {
            e.printStackTrace();
            snack = "Ocorreu um erro inesperado, tente refazer a conexão.";

        }
        if(! snack.equals(""))  //snack precisa ser diferente de "" para mostrar snackbar
        {
            Snackbar popup = Snackbar.make(findViewById(R.id.myCoordinatorLayout), snack, Snackbar.LENGTH_SHORT);
            popup.show();
        }

        return aux;

    }
    //=========================================================================================================================


    @Override
    protected void onDestroy() {
        super.onDestroy();
        servidor.close();
    }
}
