package com.example.lucas.space;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class SocketServidor extends AsyncTask <Void, String, Boolean>
{

    int port;

    public SocketServidor(int port) {
        this.port = port;
    }

    ServerSocket servidor;
    Socket socket;
    PrintWriter saida;
    BufferedReader entrada;
    String data = "...";
    String text = null;


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Boolean doInBackground(Void... voids) {

        try
        {
            servidor = new ServerSocket(port);								//cria o servidor socket
            text = "Servidor iniciado em " + servidor.getInetAddress().getHostAddress() + " na porta " + port;
            publishProgress(text);
            socket = servidor.accept();
            text = "Conectado com " + socket.getInetAddress().getHostAddress();
            publishProgress(text);
            do
            {
                entrada = new BufferedReader(new InputStreamReader(socket.getInputStream()));  //faço minima ideia
                data = entrada.readLine();
                //System.out.println(data);
                publishProgress(text);


            }while(! entrada.equals("end"));
            text = "Conexão encerrada pelo cliente.";
            publishProgress(text);
            close();
            return true;

        }catch (IOException e) {
            text = "Não foi possível iniciar o servidor, tente resolver usando outra porta.";
            return false;

        }

    }

    @Override
    protected void onProgressUpdate(String... texto) {
        super.onProgressUpdate(texto);
        MainActivity.sup.setText(texto[0] + "\n" + data);
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        MainActivity.sup.setText(text + " " + data);
    }

    public Boolean close()
    {
        try {

            saida.close();
            entrada.close();
            socket.close();
            servidor.close();
            return true;

        } catch (Exception e) {
            // TODO: handle exception
            return false;
        }


    }
}
